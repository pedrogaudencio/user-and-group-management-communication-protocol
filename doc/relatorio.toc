\select@language {portuguese}
\contentsline {section}{\numberline {1}Introdu\IeC {\c c}\IeC {\~a}o}{3}{section.1}
\contentsline {section}{\numberline {2}Compilar/Makefile}{4}{section.2}
\contentsline {section}{\numberline {3}Esquema do protocolo desenvolvido}{5}{section.3}
\contentsline {subsection}{\numberline {3.1}Cliente}{5}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1}Utilizadores (10X) - login e registo}{5}{subsubsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.2}Grupos (2XX)}{5}{subsubsection.3.1.2}
\contentsline {subsubsection}{\numberline {3.1.3}Mensagens Privadas (3XX)}{5}{subsubsection.3.1.3}
\contentsline {subsection}{\numberline {3.2}Servidor}{6}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}Sucesso (4XX)}{6}{subsubsection.3.2.1}
\contentsline {paragraph}{\numberline {3.2.1.1}Utilizadores (40X) - login e registo}{6}{paragraph.3.2.1.1}
\contentsline {paragraph}{\numberline {3.2.1.2}Grupos (41X)}{6}{paragraph.3.2.1.2}
\contentsline {subsubsection}{\numberline {3.2.2}Erro (5XX)}{6}{subsubsection.3.2.2}
\contentsline {paragraph}{\numberline {3.2.2.1}Utilizadores (50X) - login e registo}{6}{paragraph.3.2.2.1}
\contentsline {paragraph}{\numberline {3.2.2.2}Grupos (5[1-4]X)}{7}{paragraph.3.2.2.2}
\contentsline {paragraph}{\numberline {3.2.2.3}Mensagens privadas (55X)}{7}{paragraph.3.2.2.3}
\contentsline {subsection}{\numberline {3.3}Codifica\IeC {\c c}\IeC {\~a}o e justifica\IeC {\c c}\IeC {\~a}o}{8}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Esquema de funcionamento}{9}{subsection.3.4}
\contentsline {section}{\numberline {4}Funcionamento}{13}{section.4}
\contentsline {section}{\numberline {5}Conclus\IeC {\~a}o}{14}{section.5}
