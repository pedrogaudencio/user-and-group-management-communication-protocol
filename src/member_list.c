#include "member_list.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>


struct memberlist *memberlist_open(char *name)
{
	int status;
	
	struct memberlist *nl = malloc(sizeof(struct memberlist));
	nl->fd = open(name,
			O_CREAT | O_RDWR,
			S_IRUSR | S_IWUSR);
	
	if(nl->fd == -1)
	{
		free(nl);
		perror("Can't open or create file");
		return NULL;
	}
	
	status = memberlist_read_header(nl);
	if(status == -1)
	{
		free(nl);
		perror("Can't read file");
		return NULL;
	}
	
	if(status == 0)
	{
		nl->memberlist_header.head = 0;
		nl->memberlist_header.used = 0;
		nl->memberlist_header.free = 0;
		
		status = memberlist_write_header(nl);
		if(status == -1)
		{
			perror("Can't write file");
			return NULL;
		}
	}
	
	return nl;
}


int memberlist_close(struct memberlist* list)
{
	close(list->fd);
	free(list);

	return 0;
}


int memberlist_location_of_node(int x)
{
	return (x-1)*sizeof(struct membernode) + sizeof(struct memberlist_header);
}


int memberlist_read_header(struct memberlist* lst)
{
	lseek(lst->fd, 0, SEEK_SET);
	return read(lst->fd, &lst->memberlist_header, sizeof(struct memberlist_header));
}


int memberlist_write_header(struct memberlist* lst)
{
	lseek(lst->fd, 0, SEEK_SET);
	return write(lst->fd, &lst->memberlist_header, sizeof(struct memberlist_header));
}


int memberlist_read_node(struct memberlist* lst, struct membernode *n, int address)
{
	lseek(lst->fd, memberlist_location_of_node(address), SEEK_SET);
	return read(lst->fd, n, sizeof(struct membernode));
}


int memberlist_write_node(struct memberlist* lst, struct membernode *n, int address)
{
	lseek(lst->fd, memberlist_location_of_node(address), SEEK_SET);
	return write(lst->fd, n, sizeof(struct membernode));
}


int memberlist_alloc(struct memberlist* lst)
{
	
	memberlist_read_header(lst);
	
	// se exister nos para alocar
	if(lst->memberlist_header.free != 0)
	{
		int to_ret = lst->memberlist_header.free;
		struct membernode f;
		
		memberlist_read_node(lst, &f, lst->memberlist_header.free);
		lst->memberlist_header.free = f.next;
		
		memberlist_write_header(lst);
		return to_ret;
	}
	// se nao exister 
	else
	{
		lst->memberlist_header.used++;
		
		memberlist_write_header(lst);
		return lst->memberlist_header.used;
	}
}


int memberlist_insert(struct memberlist* lst, char* member)
{
	int new_address = memberlist_alloc(lst);
	
	struct membernode n_node;
	
	strcpy(n_node.member, member);
	n_node.next = lst->memberlist_header.head;
	
	lst->memberlist_header.head = new_address;
	
	memberlist_write_node(lst, &n_node, new_address);
	memberlist_write_header(lst);
	
	return 0;
}


void memberlist_clear(struct memberlist* lst)
{
	struct membernode c_node;
	memberlist_read_header(lst);
	memberlist_read_node(lst, &c_node, lst->memberlist_header.head);

	int adr_of_c_node = lst->memberlist_header.head, adr_of_prev_node = -1;
	while(c_node.next != 0)
	{
		adr_of_prev_node = adr_of_c_node;
		adr_of_c_node = c_node.next;
		memberlist_read_node(lst, &c_node, c_node.next);

	if(adr_of_prev_node != -1)
	{
		struct membernode prev;
		memberlist_read_node(lst, &prev, adr_of_prev_node);
		prev.next = c_node.next;
		memberlist_write_node(lst, &prev, adr_of_prev_node);
	}
	else
		lst->memberlist_header.head = c_node.next;

	memberlist_write_node(lst, &c_node, adr_of_c_node);
	memberlist_write_header(lst);

	}
}


void memberlist_remove(struct memberlist* lst, char* member)
{
	struct membernode c_node;
	memberlist_read_header(lst);
	memberlist_read_node(lst, &c_node, lst->memberlist_header.head);

	int adr_of_c_node = lst->memberlist_header.head, adr_of_prev_node = -1;
	while(strcmp(member, c_node.member)!=0)
	{
		adr_of_prev_node = adr_of_c_node;
		adr_of_c_node = c_node.next;
		memberlist_read_node(lst, &c_node, c_node.next);
	}

	if(adr_of_prev_node != -1)
	{
		struct membernode prev;
		memberlist_read_node(lst, &prev, adr_of_prev_node);
		prev.next = c_node.next;
		memberlist_write_node(lst, &prev, adr_of_prev_node);
	}
	else
		lst->memberlist_header.head = c_node.next;

	memberlist_write_node(lst, &c_node, adr_of_c_node);
	memberlist_write_header(lst);
}


void memberlist_print(struct memberlist* lst)
{
	struct membernode c_node;
	
	memberlist_read_header(lst);
	
	printf("Head: %d;\nUsed: %d;\nFree: %d.\n\n", lst->memberlist_header.head, lst->memberlist_header.used, lst->memberlist_header.free);
	
	printf("Nodes:\n");
	memberlist_read_node(lst, &c_node, lst->memberlist_header.head);
	while(1)
	{
		printf("U: %s N: %d\n", c_node.member, c_node.next);
		
		if(c_node.next == 0)
			break;
		
		memberlist_read_node(lst, &c_node, c_node.next);
	}
	


}


char * member_add(struct memberlist* lst, char* member)
{
	// printf("lst->memberlist_header.head: %d\n", lst->memberlist_header.head);
	if(lst->memberlist_header.head == 0){
		memberlist_insert(lst, member);

		return "412"; // member inscrito
	}

	struct membernode c_node;
	memberlist_read_header(lst);
	memberlist_read_node(lst, &c_node, lst->memberlist_header.head);

	while(1)
	{
		if(strcmp(member, c_node.member)==0){
				return "513"; // member já está inscrito
		}
		else{
			if(c_node.next == 0){
				memberlist_insert(lst, member);

				return "412"; // member inscrito
			}
		}
		
		memberlist_read_node(lst, &c_node, c_node.next);
	}
}


int member_remove(struct memberlist* lst, char* member)
{
	if(lst->memberlist_header.head == 0){
		return -1; // member não está na lista
	}

	struct membernode c_node;
	memberlist_read_header(lst);
	memberlist_read_node(lst, &c_node, lst->memberlist_header.head);

	while(1)
	{
		if(strcmp(member, c_node.member)==0){
			memberlist_remove(lst, member);
			return 0; // member removido
		}
		else{
			if(c_node.next == 0){
				return -1; // member não está na lista
			}
		}
		
		memberlist_read_node(lst, &c_node, c_node.next);
	}
}
