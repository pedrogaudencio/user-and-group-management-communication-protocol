#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>

#ifdef WIN32	// se o SO for windows
#include <winsock2.h>
WSADATA wsa_data;
#else			// se o SO for linux/unix
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#endif

int main()
{
	#ifdef WIN32	// se o SO for windows
	WSAStartup(MAKEWORD(2, 0), &wsa_data);
	#endif

	//criar o socket	--------------------------------------------------------
	int sock;
	
	// Definição e população da estructura sockaddr_in -------------------------
	struct sockaddr_in server;
	memset(&(server.sin_zero),0, sizeof(server.sin_zero));
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = inet_addr("127.0.0.1");	//define o IP
	server.sin_port = htons(2000);						//define a porta [1012-65535]

	char to_send[2048];	// variável para guardar o que é escrito do socket
	char received[2048]; // variável para guardar a resposta do servidor
	char command[4], args1[32], args2[32], args3[128], arg_received[2048], cod_receive[3];

	bool is_sending = true;

	char cod[1024]; // código e mensagem ser enviada

	while(1){
		sock = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);

		fgets(to_send, 300, stdin);

		int n_args = sscanf(to_send, "%s %s %s %s", command, args1, args2, args3);

		if(n_args == 1){
			if(strcmp("pml", command)==0){
				sprintf(cod, "%s", "300");
			}
			else {
				cod[0] = '\0';
				printf("\n");
				int print_cmd;
				FILE * command_list = fopen("usage.txt", "r");
				if(command_list){
				    while((print_cmd = getc(command_list)) != EOF)
				        putchar(print_cmd);
				    fclose(command_list);
				}
			}
		}
		else if(n_args == 2){
			if(strcmp("gc", command)==0){
				sprintf(cod, "%s %s", "200", args1);
			}
			else if(strcmp("gr", command)==0){
				sprintf(cod, "%s %s", "201", args1);
			}
			else if(strcmp("gj", command)==0){
				sprintf(cod, "%s %s", "210", args1);
			}
			else if(strcmp("gl", command)==0){
				sprintf(cod, "%s %s", "211", args1);
			}
			else if(strcmp("gml", command)==0){
				sprintf(cod, "%s %s", "230", args1);
			}
			else if(strcmp("pms", command)==0){
				sprintf(cod, "%s %s", "301", args1);
			}
			else if(strcmp("pmr", command)==0){
				sprintf(cod, "%s %s", "302", args1);
			}
			else if(strcmp("bl", command)==0){
				sprintf(cod, "%s %s", "310", args1);
			}
			else if(strcmp("blr", command)==0){
				sprintf(cod, "%s %s", "311", args1);
			}
			else{
				is_sending = false;
				printf("Comando errado.\n\n");
			}
		}
		else if(n_args == 3){
			if(strcmp("l", command)==0){
				sprintf(cod, "%s %s %s", "100", args1, args2);
			}
			else if(strcmp("r", command)==0){
				sprintf(cod, "%s %s %s", "101", args1, args2);
			}
			else if(strcmp("gaa", command)==0){
				sprintf(cod, "%s %s %s", "220", args1, args2);
			}
			else if(strcmp("gar", command)==0){
				sprintf(cod, "%s %s %s", "221", args1, args2);
			}
			else if(strcmp("gmr", command)==0){
				sprintf(cod, "%s %s %s", "233", args1, args2);
			}
			else if(strcmp("gms", command)==0){
				sprintf(cod, "%s %s %s", "231", args1, args2);
			}
			else if(strcmp("gamr", command)==0){
				sprintf(cod, "%s %s %s", "241", args1, args2);
			}
			else{
				is_sending = false;
				printf("Comando errado.\n");
			}
		}
		else if(n_args == 4){
			if(strcmp("gmp", command)==0){
				sprintf(cod, "%s %s %s %s", "232", args1, args2, args3);
			}
			else if(strcmp("gmas", command)==0){
				sprintf(cod, "%s %s %s %s", "240", args1, args2, args3);
			}
			else if(strcmp("pmp", command)==0){
				sprintf(cod, "%s %s %s %s", "301", args1, args2, args3);
			}
			else{
				is_sending = false;
				printf("Comando errado.\n");
			}
		}
		if(is_sending == true){
			int request = connect(sock, (struct sockaddr *)&server,sizeof(server));

			if(request == -1)
				printf("Conexão não encontrada.\n");
			else{
				send(sock, cod, strlen(to_send)+1, 0);
				recv(sock, received, sizeof(received)+1, 0);
				n_args = sscanf(received, "%s %s", cod_receive, arg_received);

				if(n_args == 1){
					if(cod_receive[0] == '4'){ // 4XX
						if(cod_receive[1] == '0'){ // 40X
							if(cod_receive[2] == '0'){ // 400
								printf("login ok\n");
							}
							else if(cod_receive[2] == '1'){ // 401
								printf("registo ok\n");
							}
						}
						else if(cod_receive[1] == '1'){ // 41X
							if(cod_receive[2] == '0'){ // 410
								printf("grupo criado ok\n");
							}
							else if(cod_receive[2] == '1'){ // 411
								printf("grupo removido ok\n");
							}
							else if(cod_receive[2] == '2'){ // 412
								printf("utilizador inscrito ok\n");
							}
							else if(cod_receive[2] == '3'){ // 413
								printf("inscricao cancelada ok\n");
							}
						}
						else if(cod_receive[1] == '2'){ // 42X
							if(cod_receive[2] == '0'){ // 420
								printf("moderador adicionado ok\n");
							}
							else if(cod_receive[2] == '1'){ // 421
								printf("moderador removido ok\n");
							}
						}
						else if(cod_receive[1] == '3'){ // 43X
							if(cod_receive[2] == '0'){ // 430
								printf("comando não implementado\n");
							}
							else if(cod_receive[2] == '1'){ // 431
								printf("comando não implementado\n");
							}
							else if(cod_receive[2] == '2'){ // 432
								printf("comando não implementado\n");
							}
							else if(cod_receive[2] == '3'){ // 433
								printf("comando não implementado\n");
							}
						}
						else if(cod_receive[1] == '4'){ // 44X
							if(cod_receive[2] == '0'){ // 440
								printf("comando não implementado\n");
							}
							else if(cod_receive[2] == '1'){ // 441
								printf("comando não implementado\n");
							}
						}
						else if(cod_receive[1] == '5'){ // 45X
							if(cod_receive[2] == '0'){ // 450
								printf("comando não implementado\n");
							}
							else if(cod_receive[2] == '1'){ // 451
								printf("comando não implementado\n");
							}
							else if(cod_receive[2] == '2'){ // 452
								printf("comando não implementado\n");
							}
							else if(cod_receive[2] == '3'){ // 453
								printf("comando não implementado\n");
							}
							else if(cod_receive[2] == '4'){ // 454
								printf("comando não implementado\n");
							}
							else if(cod_receive[2] == '5'){ // 455
								printf("comando não implementado\n");
							}
						}
					}
					else if(cod_receive[0] == '5'){ // 5XX
						if(cod_receive[1] == '0'){ // 50X
							if(cod_receive[2] == '0'){ // 500
								printf("operacao necessita de login\n");
							}
							else if(cod_receive[2] == '1'){ // 501
								printf("password errada\n");
							}
							else if(cod_receive[2] == '2'){ // 502
								printf("utilizador nao existe\n");
							}
							else if(cod_receive[2] == '3'){ // 503
								printf("nome de utilizador ja existe\n");
							}
						}
						else if(cod_receive[1] == '1'){ // 51X
							if(cod_receive[2] == '0'){ // 510
								printf("nome do grupo ja existe\n");
							}
							else if(cod_receive[2] == '1'){ // 511
								printf("nao tem permissoes para remover o grupo\n");
							}
							else if(cod_receive[2] == '2'){ // 512
								printf("grupo inexistente\n");
							}
							else if(cod_receive[2] == '3'){ // 513
								printf("utilizador ja esta inscrito\n");
							}
							else if(cod_receive[2] == '4'){ // 514
								printf("nao esta inscrito nesse grupo\n");
							}
							else if(cod_receive[2] == '5'){ // 515
								printf("o administrador nao se pode desinscrever do proprio grupo\n");
							}
						}
						else if(cod_receive[1] == '2'){ // 52X
							if(cod_receive[2] == '0'){ // 520
								printf("nao tem permissoes para promover utilizador a moderador neste grupo\n");
							}
							else if(cod_receive[2] == '1'){ // 521
								printf("o utilizador não e' moderador deste grupo\n");
							}
						}
						else if(cod_receive[1] == '3'){ // 53X
							if(cod_receive[2] == '0'){ // 530
								printf("comando não implementado\n");
							}
							else if(cod_receive[2] == '1'){ // 531
								printf("comando não implementado\n");
							}
							else if(cod_receive[2] == '2'){ // 532
								printf("comando não implementado\n");
							}
							else if(cod_receive[2] == '3'){ // 533
								printf("comando não implementado\n");
							}
						}
						else if(cod_receive[1] == '4'){ // 54X
							if(cod_receive[2] == '0'){ // 540
								printf("comando não implementado\n");
							}
							else if(cod_receive[2] == '1'){ // 541
								printf("comando não implementado\n");
							}
						}
						else if(cod_receive[1] == '5'){ // 55X
							if(cod_receive[2] == '0'){ // 550
								printf("comando não implementado\n");
							}
							else if(cod_receive[2] == '1'){ // 551
								printf("comando não implementado\n");
							}
							else if(cod_receive[2] == '2'){ // 552
								printf("comando não implementado\n");
							}
							else if(cod_receive[2] == '3'){ // 553
								printf("comando não implementado\n");
							}
							else if(cod_receive[2] == '4'){ // 554
								printf("comando não implementado\n");
							}
							else if(cod_receive[2] == '5'){ // 555
								printf("comando não implementado\n");
							}
						}
					}
				}
				else if(n_args == 2){

				}
			}
		send(sock, cod,strlen(cod)+1, 0);
		strcpy(cod,"\0");
		printf("\n");
		}

	//is_sending = true;
	close(sock);
	}

}