#include "user_list.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>


struct userlist *userlist_open(char *name)
{
	int status;
	
	struct userlist *nl = malloc(sizeof(struct userlist));
	nl->fd = open(name,
			O_CREAT | O_RDWR,
			S_IRUSR | S_IWUSR);
	
	if(nl->fd == -1)
	{
		free(nl);
		perror("Can't open or create file");
		return NULL;
	}
	
	status = userlist_read_header(nl);
	if(status == -1)
	{
		free(nl);
		perror("Can't read file");
		return NULL;
	}
	
	if(status == 0)
	{
		nl->userlist_header.head = 0;
		nl->userlist_header.used = 0;
		nl->userlist_header.free = 0;
		
		status = userlist_write_header(nl);
		if(status == -1)
		{
			perror("Can't write file");
			return NULL;
		}
	}
	
	return nl;
}


int userlist_close(struct userlist* list)
{
	close(list->fd);
	free(list);

	return 0;
}


int userlist_location_of_node(int x)
{
	return (x-1)*sizeof(struct usernode) + sizeof(struct userlist_header);
}


int userlist_read_header(struct userlist* lst)
{
	lseek(lst->fd, 0, SEEK_SET);
	return read(lst->fd, &lst->userlist_header, sizeof(struct userlist_header));
}


int userlist_write_header(struct userlist* lst)
{
	lseek(lst->fd, 0, SEEK_SET);
	return write(lst->fd, &lst->userlist_header, sizeof(struct userlist_header));
}


int userlist_read_node(struct userlist* lst, struct usernode *n, int address)
{
	lseek(lst->fd, userlist_location_of_node(address), SEEK_SET);
	return read(lst->fd, n, sizeof(struct usernode));
}


int userlist_write_node(struct userlist* lst, struct usernode *n, int address)
{
	lseek(lst->fd, userlist_location_of_node(address), SEEK_SET);
	return write(lst->fd, n, sizeof(struct usernode));
}


int userlist_alloc(struct userlist* lst)
{
	
	userlist_read_header(lst);
	
	// se exister nos para alocar
	if(lst->userlist_header.free != 0)
	{
		int to_ret = lst->userlist_header.free;
		struct usernode f;
		
		userlist_read_node(lst, &f, lst->userlist_header.free);
		lst->userlist_header.free = f.next;
		
		userlist_write_header(lst);
		return to_ret;
	}
	// se nao exister 
	else
	{
		lst->userlist_header.used++;
		
		userlist_write_header(lst);
		return lst->userlist_header.used;
	}
}


int userlist_insert(struct userlist* lst, char* user, char* pass)
{
	int new_address = userlist_alloc(lst);
	
	struct usernode n_node;
	
	strcpy(n_node.username, user);
	strcpy(n_node.password, pass);
	n_node.next = lst->userlist_header.head;
	
	lst->userlist_header.head = new_address;
	
	userlist_write_node(lst, &n_node, new_address);
	userlist_write_header(lst);
	
	return 0;
}


int userlist_remove(struct userlist* lst, char* user)
{

	struct usernode c_node;
	userlist_read_header(lst);
	userlist_read_node(lst, &c_node, lst->userlist_header.head);
	

	int adr_of_c_node = lst->userlist_header.head, adr_of_prev_node = -1;
	while(strcmp(user, c_node.username)!=0)
	{
		adr_of_prev_node = adr_of_c_node;
		adr_of_c_node = c_node.next;
		userlist_read_node(lst, &c_node, c_node.next);
	}
	

	if(adr_of_prev_node != -1)
	{
		struct usernode prev;
		userlist_read_node(lst, &prev, adr_of_prev_node);
		prev.next = c_node.next;
		userlist_write_node(lst, &prev, adr_of_prev_node);
	}
	else
		lst->userlist_header.head = c_node.next;
	

	userlist_write_node(lst, &c_node, adr_of_c_node);
	userlist_write_header(lst);

	return 0;
}


void userlist_print(struct userlist* lst)
{
	struct usernode c_node;
	
	userlist_read_header(lst);
	
	printf("Head: %d;\nUsed: %d;\nFree: %d.\n\n", lst->userlist_header.head, lst->userlist_header.used, lst->userlist_header.free);
	
	printf("Nodes:\n");
	userlist_read_node(lst, &c_node, lst->userlist_header.head);
	while(1)
	{
		printf("U: %s P: %s N: %d\n", c_node.username, c_node.password, c_node.next);
		
		if(c_node.next == 0)
			break;
		
		userlist_read_node(lst, &c_node, c_node.next);
	}
	


}


char * user_identify(struct userlist* lst, char* user, char* pass)
{
	if(lst->userlist_header.head == 0)
		return "502"; // username não existe

	struct usernode c_node;
	userlist_read_header(lst);
	userlist_read_node(lst, &c_node, lst->userlist_header.head);

	while(1)
	{
		printf("U: %s P: %s N: %d\n", c_node.username, c_node.password, c_node.next);

		if(strcmp(user, c_node.username)==0){
			if(strcmp(pass, c_node.password)==0)
				return "400"; // password aceite
			else
				return "501"; // password errada
		}
		else{
			if(c_node.next == 0)
				return "502"; // username não existe
		}
		
		userlist_read_node(lst, &c_node, c_node.next);
	}
	// depois de retornar isto se a password estiver certa vai guardar o username e meter o is_logged_in a true, senão não guarda nada e não está logado
}


char * user_register(struct userlist* lst, char* user, char* pass)
{
	if(lst->userlist_header.head == 0){
		userlist_insert(lst, user, pass);

		return "401"; // user criado
	}

	struct usernode c_node;
	userlist_read_header(lst);
	userlist_read_node(lst, &c_node, lst->userlist_header.head);

	while(1)
	{
		printf("U: %s P: %s N: %d\n", c_node.username, c_node.password, c_node.next);

		if(strcmp(user, c_node.username)==0){
				return "503"; // user já existe
		}
		else{
			if(c_node.next == 0){
				userlist_insert(lst, user, pass);

				return "401"; // user criado
			}
		}
		
		userlist_read_node(lst, &c_node, c_node.next);
	}
}
