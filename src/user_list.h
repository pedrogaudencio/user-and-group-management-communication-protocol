#define NIL 0

/* Cabeçalho de uma lista */
struct userlist_header
{
	int head; // cabeça da lista
	int used; // número de nós já usados
	int free; // cabeça da lista dos nós livres
};

/* Lista */
struct userlist
{
	int fd; // descritor do ficheiro onde a lista reside
	struct userlist_header userlist_header; // cabeçalho da lista
};

/* Nó */
struct usernode
{
	char username[32]; // username
	char password[32]; // password
	int next; // próximo nó da lista
};

struct userlist *userlist_open(char *name);
int userlist_close(struct userlist *lst);

int userlist_location_of_node(int x);

int userlist_read_header(struct userlist *lst);
int userlist_write_header(struct userlist *lst);

int userlist_read_node(struct userlist *lst, struct usernode *n, int address);
int userlist_write_node(struct userlist *lst, struct usernode *n, int address);

int userlist_alloc(struct userlist *lst);
void userlist_free(struct userlist *lst, int adr_to_free);

int userlist_insert(struct userlist *lst, char* user, char* pass);
int userlist_remove(struct userlist *lst, char* user);

void userlist_print(struct userlist *lst);

char* user_identify(struct userlist* lst, char* user, char* pass);
char* user_register(struct userlist* lst, char* user, char* pass);