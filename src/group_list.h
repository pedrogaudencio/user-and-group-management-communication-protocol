#define NIL 0

/* Cabeçalho de uma lista */
struct grouplist_header
{
	int head; // cabeça da lista
	int used; // número de nós já usados
	int free; // cabeça da lista dos nós livres
};

/* Lista */
struct grouplist
{
	int fd; // descritor do ficheiro onde a lista reside
	struct grouplist_header grouplist_header; // cabeçalho da lista
};

/* Nó */
struct groupnode
{
	char groupname[32]; // group name
	char groupadmin[32]; // group admin
	struct memberlist * group_moderators;
	struct memberlist * group_members;
	struct messagelist * group_messages;
	int next; // próximo nó da lista
};

struct grouplist *grouplist_open(char *name);
int grouplist_close(struct grouplist *lst);

int grouplist_location_of_node(int x);

int grouplist_read_header(struct grouplist *lst);
int grouplist_write_header(struct grouplist *lst);

int grouplist_read_node(struct grouplist *lst, struct groupnode *n, int address);
int grouplist_write_node(struct grouplist *lst, struct groupnode *n, int address);

int grouplist_alloc(struct grouplist *lst);
void grouplist_free(struct grouplist *lst, int adr_to_free);

int grouplist_insert(struct grouplist* lst, char* group, char* admin);
int grouplist_remove(struct grouplist *lst, char* group);

void grouplist_print(struct grouplist *lst);

char * group_create(struct grouplist* lst, char* group, char* admin);
char * group_remove(struct grouplist* lst, char* group, char* user);

char * group_register(struct grouplist* lst, char* group, char* user);
char * group_leave(struct grouplist* lst, char* group, char* user);

char * group_moderator_add(struct grouplist* lst, char* group, char* admin, char* moderator);
char * group_moderator_remove(struct grouplist* lst, char* group, char* admin, char* moderator);

/*
char * group_post_message(struct grouplist* lst, char* group, char* subject, char* body, bool is_admin_msg);
char * group_remove_message(struct grouplist* lst, char* group, char* user, char* subject, bool is_admin_msg);
char * group_list_messages(struct grouplist* lst, char* group, char* user);
char * group_list_message_n(struct grouplist* lst, char* group, char* user, char* subject);
*/