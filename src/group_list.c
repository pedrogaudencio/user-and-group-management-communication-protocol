#include "group_list.h"
#include "member_list.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>


struct grouplist *grouplist_open(char *name)
{
	int status;

	struct grouplist *nl = malloc(sizeof(struct grouplist));
	nl->fd = open(name,
			O_CREAT | O_RDWR,
			S_IRUSR | S_IWUSR);
	
	if(nl->fd == -1)
	{
		free(nl);
		perror("Can't open or create file");
		return NULL;
	}
	
	status = grouplist_read_header(nl);
	if(status == -1)
	{
		free(nl);
		perror("Can't read file");
		return NULL;
	}
	
	if(status == 0)
	{
		nl->grouplist_header.head = 0;
		nl->grouplist_header.used = 0;
		nl->grouplist_header.free = 0;
		
		status = grouplist_write_header(nl);
		if(status == -1)
		{
			perror("Can't write file");
			return NULL;
		}
	}

	return nl;
}


int grouplist_close(struct grouplist* list)
{
	close(list->fd);
	free(list);

	return 0;
}


int grouplist_location_of_node(int x)
{
	return (x-1)*sizeof(struct groupnode) + sizeof(struct grouplist_header);
}


int grouplist_read_header(struct grouplist* lst)
{
	lseek(lst->fd, 0, SEEK_SET);
	return read(lst->fd, &lst->grouplist_header, sizeof(struct grouplist_header));
}


int grouplist_write_header(struct grouplist* lst)
{
	lseek(lst->fd, 0, SEEK_SET);
	return write(lst->fd, &lst->grouplist_header, sizeof(struct grouplist_header));
}


int grouplist_read_node(struct grouplist* lst, struct groupnode *n, int address)
{
	lseek(lst->fd, grouplist_location_of_node(address), SEEK_SET);
	return read(lst->fd, n, sizeof(struct groupnode));
}


int grouplist_write_node(struct grouplist* lst, struct groupnode *n, int address)
{
	lseek(lst->fd, grouplist_location_of_node(address), SEEK_SET);
	return write(lst->fd, n, sizeof(struct groupnode));
}


int grouplist_alloc(struct grouplist* lst)
{
	
	grouplist_read_header(lst);
	
	// se exister nos para alocar
	if(lst->grouplist_header.free != 0)
	{
		int to_ret = lst->grouplist_header.free;
		struct groupnode f;
		
		grouplist_read_node(lst, &f, lst->grouplist_header.free);
		lst->grouplist_header.free = f.next;
		
		grouplist_write_header(lst);
		return to_ret;
	}
	// se nao exister 
	else
	{
		lst->grouplist_header.used++;
		
		grouplist_write_header(lst);
		return lst->grouplist_header.used;
	}
}


int grouplist_insert(struct grouplist* lst, char* group, char* admin)
{
	int new_address = grouplist_alloc(lst);
	
	struct groupnode n_node;
	
	strcpy(n_node.groupname, group);
	strcpy(n_node.groupadmin, admin);

	char * memb_append = "_memberlist.dat";
	char * memb = malloc(sizeof(group) + sizeof(memb_append)+10);
	strcat(memb,group);
	strcat(memb,memb_append);
	// printf("memb: %s\n", memb);

	char * mod_append = "_moderatorlist.dat";
	char * mod = malloc(sizeof(group) + sizeof(mod_append)+10);
	strcat(mod,group);
	strcat(mod,mod_append);
	// printf("mod: %s\n", mod);

	n_node.group_members = memberlist_open(memb);
	n_node.group_moderators = memberlist_open(mod);

	member_add(n_node.group_moderators, admin);
	member_add(n_node.group_members, admin);

	/*
	printf("n_node.group_moderators:\n");
	memberlist_print(n_node.group_moderators);

	printf("n_node.group_members:\n");
	memberlist_print(n_node.group_members);
	*/

	n_node.next = lst->grouplist_header.head;
	
	lst->grouplist_header.head = new_address;
	
	grouplist_write_node(lst, &n_node, new_address);
	grouplist_write_header(lst);
	
	return 0;
}


int grouplist_remove(struct grouplist* lst, char* group)
{

	struct groupnode c_node;
	grouplist_read_header(lst);
	grouplist_read_node(lst, &c_node, lst->grouplist_header.head);
	

	int adr_of_c_node = lst->grouplist_header.head, adr_of_prev_node = -1;
	while(strcmp(group, c_node.groupname)!=0)
	{
		adr_of_prev_node = adr_of_c_node;
		adr_of_c_node = c_node.next;
		grouplist_read_node(lst, &c_node, c_node.next);
	}

	//memberlist_clear(c_node.group_members);
	//memberlist_clear(c_node.group_moderators);

	char * memb_append = "_memberlist.dat";
	char * memb = malloc(sizeof(group) + sizeof(memb_append)+10);
	strcat(memb,group);
	strcat(memb,memb_append);
	remove(memb);

	char * mod_append = "_moderatorlist.dat";
	char * mod = malloc(sizeof(group) + sizeof(mod_append)+10);
	strcat(mod,group);
	strcat(mod,mod_append);
	remove(mod);

	if(adr_of_prev_node != -1)
	{
		struct groupnode prev;
		grouplist_read_node(lst, &prev, adr_of_prev_node);
		prev.next = c_node.next;
		grouplist_write_node(lst, &prev, adr_of_prev_node);
	}
	else
		lst->grouplist_header.head = c_node.next;

	grouplist_write_node(lst, &c_node, adr_of_c_node);
	grouplist_write_header(lst);

	return 0;
}


void grouplist_print(struct grouplist* lst)
{
	struct groupnode c_node;
	
	grouplist_read_header(lst);
	
	printf("Head: %d;\nUsed: %d;\nFree: %d.\n\n", lst->grouplist_header.head, lst->grouplist_header.used, lst->grouplist_header.free);
	
	printf("Nodes:\n");
	grouplist_read_node(lst, &c_node, lst->grouplist_header.head);
	while(1)
	{
		printf("G: %s N: %d\n", c_node.groupname, c_node.next);
		
		if(c_node.next == 0)
			break;
		
		grouplist_read_node(lst, &c_node, c_node.next);
	}
	


}


char * group_create(struct grouplist* lst, char* group, char* admin)
{
	if(lst->grouplist_header.head == 0){
		grouplist_insert(lst, group, admin);

		return "410"; // group criado
	}

	struct groupnode c_node;
	grouplist_read_header(lst);
	grouplist_read_node(lst, &c_node, lst->grouplist_header.head);

	while(1)
	{
		// printf("U: %s N: %d\n", c_node.groupname, c_node.next);

		if(strcmp(group, c_node.groupname)==0)
				return "510"; // group já existe
		else{
			if(c_node.next == 0){
				grouplist_insert(lst, group, admin);

				return "410"; // group criado
			}
		}
		
		grouplist_read_node(lst, &c_node, c_node.next);
	}
}


char * group_remove(struct grouplist* lst, char* group, char* admin)
{
	if(lst->grouplist_header.head == 0){
		return "512"; // grupo não existe
	}

	struct groupnode c_node;
	grouplist_read_header(lst);
	grouplist_read_node(lst, &c_node, lst->grouplist_header.head);

	while(1)
	{
		// printf("U: %s N: %d\n", c_node.groupname, c_node.next);

		if(strcmp(group, c_node.groupname)==0){
			if(strcmp(admin, c_node.groupadmin)==0){
				grouplist_remove(lst, c_node.groupname);
				return "411"; // grupo removido
			}
			else
				return "511"; // não tem permissões para remover o grupo
		}
		else{
			if(c_node.next == 0){
				return "512"; // grupo não existe
			}
		}
		
		grouplist_read_node(lst, &c_node, c_node.next);
	}
}


char * group_register(struct grouplist* lst, char* group, char* user)
{
	if(lst->grouplist_header.head == 0){
		return "512"; // grupo não existe
	}

	struct groupnode c_node;
	grouplist_read_header(lst);
	grouplist_read_node(lst, &c_node, lst->grouplist_header.head);

	while(1)
	{
		//printf("G: %s\n", c_node.groupname);

		if(strcmp(group, c_node.groupname)==0){
			return member_add(c_node.group_members, user);
		}
		else{
			if(c_node.next == 0)
				return "512"; // grupo não existe
		}
		
		grouplist_read_node(lst, &c_node, c_node.next);
	}
}


char * group_leave(struct grouplist* lst, char* group, char* user)
{
	if(lst->grouplist_header.head == 0){
		return "512"; // grupo não existe
	}

	struct groupnode c_node;
	grouplist_read_header(lst);
	grouplist_read_node(lst, &c_node, lst->grouplist_header.head);

	memberlist_print(c_node.group_members);

	while(1)
	{
		//printf("U: %s N: %d\n", c_node.groupname, c_node.next);

		if(strcmp(group, c_node.groupname)==0){
			if(strcmp(user, c_node.groupadmin)!=0){
				int r = member_remove(c_node.group_members, user);
				if(r==0)
					return "413"; // user removido
				else
					if(r == -1)
						return "514"; // não está inscrito neste grupo
				break;
			}
			else
				if(strcmp(c_node.groupadmin, user)==0)
					return "515"; // o admin não se pode desinscrever do próprio grupo
				else
					return "514"; // não está inscrito nesse grupo
		}
		else{
			if(c_node.next == 0)
				return "512"; // grupo não existe
		}
		
		grouplist_read_node(lst, &c_node, c_node.next);
	}
	return "512";
}


char * group_moderator_add(struct grouplist* lst, char* group, char* admin, char* moderator)
{
	if(lst->grouplist_header.head == 0){
		return "512"; // grupo não existe
	}

	struct groupnode c_node;
	grouplist_read_header(lst);
	grouplist_read_node(lst, &c_node, lst->grouplist_header.head);

	while(1)
	{
		//printf("G: %s\n", c_node.groupname);

		if(strcmp(group, c_node.groupname)==0){
			if(strcmp(admin, c_node.groupadmin)==0){

				struct membernode m_node;
				memberlist_read_header(c_node.group_moderators);
				memberlist_read_node(c_node.group_moderators, &m_node, c_node.group_moderators->memberlist_header.head);

				while(1)
				{
					if(strcmp(moderator, m_node.member)==0){
							return "513"; // moderador já está inscrito
					}
					else{
						if(m_node.next == 0){
							memberlist_insert(c_node.group_moderators, moderator);
							return "420"; // moderador adicionado
						}
					}
					
					memberlist_read_node(c_node.group_moderators, &m_node, m_node.next);
				}
			}
			else
				return "520"; // não tem permissões para fazer essa operação
		}
		else{
			if(c_node.next == 0)
				return "512"; // grupo não existe
		}
		
		grouplist_read_node(lst, &c_node, c_node.next);
	}
}


char * group_moderator_remove(struct grouplist* lst, char* group, char* admin, char* moderator)
{
	if(lst->grouplist_header.head == 0){
		return "512"; // grupo não existe
	}

	struct groupnode c_node;
	grouplist_read_header(lst);
	grouplist_read_node(lst, &c_node, lst->grouplist_header.head);

	while(1)
	{
		// printf("U: %s N: %d\n", c_node.groupname, c_node.next);

		if(strcmp(group, c_node.groupname)==0){
			if(strcmp(c_node.groupadmin, admin))
					return "515"; // o admin não se pode desinscrever do próprio grupo

			int r = member_remove(c_node.group_moderators, moderator);
			if(r==0)
				return "421"; // moderador removido
			else
				if(r == -1)
					return "521"; // não está inscrito neste grupo
		}
		else{
			if(c_node.next == 0)
				return "512"; // grupo não existe
		}
		
		grouplist_read_node(lst, &c_node, c_node.next);
	}
}

/*
char * group_post_message(struct grouplist* lst, char* group, char* subject, char* body, bool is_admin_msg){

}


char * group_remove_message(struct grouplist* lst, char* group, char* user, char* subject, bool is_admin_msg){

}


char * group_list_messages(struct grouplist* lst, char* group, char* user){
	if(lst->grouplist_header.head == 0){
		return "512"; // grupo não existe
	}

	struct groupnode c_node;
	grouplist_read_header(lst);
	grouplist_read_node(lst, &c_node, lst->grouplist_header.head);
	char* m;

	while(1)
	{
		if(strcmp(group, c_node.groupname)==0){
			struct membernode m_node;
			memberlist_read_header(c_node.group_members);
			memberlist_read_node(c_node.group_members, &m_node, c_node.group_members->memberlist_header.head);

			while(1)
			{
				if(strcmp(user, m_node.member)==0){
						strcat(m, ""); // adicionar código de msg ok
						strcat(m, message_list_all(c_node.group_messages, true)); // falta ver se o user é admin
						return m;
				}
				else{
					if(m_node.next == 0){
						return "514"; // não está inscrito no grupo
					}
				}
				
				memberlist_read_node(c_node.group_moderators, &m_node, m_node.next);
			}
		}
		else{
			if(c_node.next == 0)
				return "512"; // grupo não existe
		}

		grouplist_read_node(lst, &c_node, c_node.next);
	}
}


char * group_list_message_n(struct grouplist* lst, char* group, char* user, char* subject){

}
*/