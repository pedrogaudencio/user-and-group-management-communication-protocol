#define NIL 0

/* Cabeçalho de uma lista */
struct memberlist_header
{
	int head; // cabeça da lista
	int used; // número de nós já usados
	int free; // cabeça da lista dos nós livres
};

/* Lista */
struct memberlist
{
	int fd; // descritor do ficheiro onde a lista reside
	struct memberlist_header memberlist_header; // cabeçalho da lista
};

/* Nó */
struct membernode
{
	char member[32]; // membername
	int next; // próximo nó da lista
};

struct memberlist *memberlist_open(char *name);
int memberlist_close(struct memberlist *lst);

int memberlist_location_of_node(int x);

int memberlist_read_header(struct memberlist *lst);
int memberlist_write_header(struct memberlist *lst);

int memberlist_read_node(struct memberlist *lst, struct membernode *n, int address);
int memberlist_write_node(struct memberlist *lst, struct membernode *n, int address);

int memberlist_alloc(struct memberlist *lst);
void memberlist_free(struct memberlist *lst, int adr_to_free);

int memberlist_insert(struct memberlist* lst, char* member);
void memberlist_remove(struct memberlist* lst, char* member);

void memberlist_print(struct memberlist *lst);

char* member_add(struct memberlist* lst, char* member);
int member_remove(struct memberlist* lst, char* member);

