#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>

#ifdef WIN32		// se o SO for windows
#include <winsock2.h>
WSADATA wsa_data;
#else				// se o SO for linux/unix
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#endif

#include "user_list.h"
#include "group_list.h"
#include "member_list.h"


struct userlist * sync_users(){
	struct userlist * ul = userlist_open("userlist.dat");
	if(ul == NULL)
	{
		perror("Couln't open userlist.\n");
	}

	return ul;
}

struct grouplist * sync_groups(){
	struct grouplist * gl = grouplist_open("grouplist.dat");
	if(gl == NULL)
	{
		perror("Couln't open grouplist.\n");
	}

	return gl;
}



int main() {
	#ifdef WIN32	// se o SO for windows
	WSAStartup(MAKEWORD(2, 0), &wsa_data);
	#endif

	struct sockaddr_in cliente; // preciso de outro para o cliente

	// Definição e população da estructura sockaddr_in -------------------------
	struct sockaddr_in servidor;
	memset(&(servidor.sin_zero),0, sizeof(servidor.sin_zero));
	servidor.sin_family = AF_INET;
	servidor.sin_addr.s_addr = htonl(INADDR_ANY);
	servidor.sin_port = htons(2000); //servidor na porta 2000

	//criar o socket	--------------------------------------------------------
	int sock = socket(AF_INET,SOCK_STREAM,0);

	//fazer o bind	------------------------------------------------------------
	int bindResult = bind(sock, (struct sockaddr *) &servidor, sizeof(servidor));
	if (bindResult==-1) // verifica se houve erros no bind
		printf("Bind: Falhou!\n");
	else
		printf("Bind: ok!\n");

	// fazer o listen com uma fila de 1 cliente no máximo ----------------------
	int tamanho_fila = 1;
	if (listen(sock,tamanho_fila) == -1)  // verifica se o listen falhou
		printf("Listen: Falhou!\n");
	else
		printf("Listen: ok!\n");


	struct userlist * userlist = sync_users();
	struct grouplist * grouplist = sync_groups();

	bool login = false;
	char user[32] = "";
	char cod_send[3];

	char command[4], args1[32], args2[32]; //, args3[128];
	char received[2048];	// variável para guardar o que é lido do socket
	char to_send[2048];		// variável para guardar a resposta para o cliente

	int nbytes;				// guarda o número de bytes da mensagem lida
	socklen_t tamanho_cliente;	// guarda o tamanho da estructura do 
	int sock_aceite;		// guarda socket aceite



	tamanho_cliente = sizeof(struct sockaddr_in); //por imposição do accept temos que fazer isto

	while(1){ // o servidor vai estar sempre a correr

		// fica à espera que um cliente se ligue
		sock_aceite = accept(sock, (struct sockaddr *)&cliente, &tamanho_cliente);

		if(sock_aceite>-1) {	//se recebeu um cliente
			//mostra informações do cliente
			printf("Recebida uma ligação do cliente IP: %s\n",inet_ntoa(cliente.sin_addr));

			// lê uma mensagem do socket
			nbytes = recv(sock_aceite, received, sizeof(received)+1, 0);
			if(nbytes>0) {
				printf("Mensagem recebida: \"%s\"\n", received);

				if(received[0] == '1'){ // 1XX login e registo
					if(received[2] == '0'){ // 100 login
						sscanf(received,"%s %s %s", command, args1, args2);
						strcpy(cod_send, user_identify(userlist, args1, args2));
						if(strcmp(cod_send, "400")==0){
							strcpy(user, args1);
							login = true;
						}
						sprintf(to_send, "%s", cod_send);
					}
					else if(received[2] == '1'){ // 101 registo
							sscanf(received,"%s %s %s", command, args1, args2);
							strcpy(cod_send, user_register(userlist, args1, args2));
							if(strcmp(cod_send, "401")==0){
								strcpy(user, args1);
								login = true;
							}
							sprintf(to_send, "%s", cod_send);
						}
				}
				else {
					if(!login){
						strcpy(cod_send, "500"); // precisa de login
						sprintf(to_send, "%s", cod_send);
					}
					else{
						if(received[0] == '2'){ // 2XX grupos
							if(received[1] == '0'){ // 20X manutenção dos grupos
								if(received[2] == '0'){ // 200 criar grupo
									sscanf(received,"%s %s", command, args1);
									strcpy(cod_send, group_create(grouplist, args1, user));
									sprintf(to_send, "%s", cod_send);
								}
								else
									if(received[2] == '1'){ // 201 remover grupo
										sscanf(received,"%s %s", command, args1);
										strcpy(cod_send, group_remove(grouplist, args1, user));
										sprintf(to_send, "%s", cod_send);
									}
							}
							else if(received[1] == '1'){ // 21X 
									if(received[2] == '0'){ // 210 adicionar utilizador
										sscanf(received,"%s %s", command, args1);
										strcpy(cod_send, group_register(grouplist, args1, user));
										sprintf(to_send, "%s", cod_send);
									}
									else if(received[2] == '1'){ // 211 remover utilizador
											sscanf(received,"%s %s", command, args1);
											strcpy(cod_send, group_leave(grouplist, args1, user));
											sprintf(to_send, "%s", cod_send);
									}
							}
							else if(received[1] == '2'){ // 22X moderadores do grupo
								if(received[2] == '0'){ // 220 adicionar moderador
									sscanf(received,"%s %s %s", command, args1, args2);
									strcpy(cod_send, group_moderator_add(grouplist, args1, user, args2));
									sprintf(to_send, "%s", cod_send);
								}
								else if(received[2] == '1'){ // 221 remover moderador
									sscanf(received,"%s %s %s", command, args1, args2);
									strcpy(cod_send, group_moderator_remove(grouplist, args1, user, args2));
									sprintf(to_send, "%s", cod_send);
								}
							}
							else if(received[1] == '3'){ // 23X mensagens de grupo
								if(received[2] == '0'){ // 230 listar mensagens do grupo
									strcpy(cod_send, "530");
									sprintf(to_send, "%s", cod_send);
								}
								else if(received[2] == '1'){ // 231 lista todas as mensagens do grupo com aquele assunto
									strcpy(cod_send, "531");
									sprintf(to_send, "%s", cod_send);
								}
								else if(received[2] == '2'){ // 232 publica mensagem no grupo
									strcpy(cod_send, "532");
									sprintf(to_send, "%s", cod_send);
								}
								else if(received[2] == '3'){ // 233 remove primeira mensagem do grupo com aquele assunto
									strcpy(cod_send, "533");
									sprintf(to_send, "%s", cod_send);
								}
							}
							else if(received[1] == '4'){ // 24X mensagens de administração de grupo
								if(received[2] == '0'){ // 240 publica mensagem administrativa no grupo
									strcpy(cod_send, "540");
									sprintf(to_send, "%s", cod_send);
								}
								else if(received[2] == '1'){ // 241 remove primeira mensagem administrativa do grupo com aquele assunto
									strcpy(cod_send, "541");
									sprintf(to_send, "%s", cod_send);
								}
							}
						}
						else {
							if(received[0] == '3'){ // 3XX mensagens privadas
								if(received[1] == '0'){ // 30X manutenção de mensagens
									if(received[2] == '0'){ // 300 lista todas as mensagens privadas do utilizador
										strcpy(cod_send, "550");
										sprintf(to_send, "%s", cod_send);
									}
									else
										if(received[2] == '1'){ // 301 envia mensagem privada para utilizador
											strcpy(cod_send, "551");
											sprintf(to_send, "%s", cod_send);
										}
										else
											if(received[2] == '2'){ // 302 remove primeira mensagem privada com aquele assunto
												strcpy(cod_send, "552");
												sprintf(to_send, "%s", cod_send);
											}
											else
												if(received[2] == '3'){ // 303 lista todas as mensagens privadas com aquele assunto
													strcpy(cod_send, "553");
													sprintf(to_send, "%s", cod_send);
												}
								}
								else if(received[1] == '1'){ // 31X mensagens e utilizadores
									if(received[2] == '0'){ // 310 bloqueia utilizador
										strcpy(cod_send, "554");
										sprintf(to_send, "%s", cod_send);
									}
									else if(received[2] == '1'){ // 311 desbloqueia utilizador
										strcpy(cod_send, "555");
										sprintf(to_send, "%s", cod_send);
									}
								}
							}
						}
					}
				}
			}
			printf("Mensagem enviada: %s\n", to_send);
			send(sock_aceite, to_send, strlen(to_send)+1, 0);
			close(sock_aceite);
		}
	}
}